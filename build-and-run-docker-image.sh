#!/bin/bash
echo "Start build docker image"

./mvnw clean compile package -Dmaven.test.skip=true
docker build -t iam-provider-api .

echo "End build docker image"

echo "Start docker compose up"

docker-compose up -d

echo "End docker compose up"
