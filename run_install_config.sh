#!/bin/bash
echo "Start run_install_config"

echo "Execute install-ubuntu-prerequisite-library.sh"
./install-ubuntu-prerequisite-library.sh

echo "Execute build-and-run-docker-image.sh"
./build-and-run-docker-image.sh

echo "End run_install_config"
