package com.acme.component.iam.provider.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "user_role_app")
public class UserRoleApp implements Serializable {

	private static final long serialVersionUID = -3789416835224320787L;

	@Id
	@Column(name = "id")
	private Long id;

	@JoinColumn(name = "user_app_id")
	@OneToOne(fetch = FetchType.LAZY)
	private UserApp userApp;

	@JoinColumn(name = "role_app_id")
	@OneToOne(fetch = FetchType.LAZY)
	private RoleApp roleApp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserApp getUserApp() {
		return userApp;
	}

	public void setUserApp(UserApp userApp) {
		this.userApp = userApp;
	}

	public RoleApp getRoleApp() {
		return roleApp;
	}

	public void setRoleApp(RoleApp roleApp) {
		this.roleApp = roleApp;
	}

	@Override
	public String toString() {
		return "UserRoleApp [id=" + id + ", userApp=" + userApp.toString() + ", roleApp=" + roleApp.toString() + "]";
	}

}
