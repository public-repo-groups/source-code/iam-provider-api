package com.acme.component.iam.provider.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "user_group_app")
public class UserGroupApp implements Serializable {

	private static final long serialVersionUID = -6930957699874393419L;

	@Id
	@Column(name = "id")
	private Long id;

	@JoinColumn(name = "user_app_id")
	@OneToOne(fetch = FetchType.LAZY)
	private UserApp userApp;

	@JoinColumn(name = "group_app_id")
	@OneToOne(fetch = FetchType.LAZY)
	private GroupApp groupApp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserApp getUserApp() {
		return userApp;
	}

	public void setUserApp(UserApp userApp) {
		this.userApp = userApp;
	}

	public GroupApp getGroupApp() {
		return groupApp;
	}

	public void setGroupApp(GroupApp groupApp) {
		this.groupApp = groupApp;
	}

	@Override
	public String toString() {
		return "UserRoleApp [id=" + id + ", userApp=" + userApp.toString() + ", groupApp=" + groupApp.toString() + "]";
	}

}
