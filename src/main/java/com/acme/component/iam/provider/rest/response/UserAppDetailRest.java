package com.acme.component.iam.provider.rest.response;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class UserAppDetailRest implements Serializable {

	private static final long serialVersionUID = 2652081365048550161L;

	private Long userId;
	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	private List<String> roles;
	private List<String> groups;
	private List<Map<String, String>> attributes;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<Map<String, String>> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Map<String, String>> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "UserAppDetailRest [userId=" + userId + ", userName=" + userName + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", roles=" + roles + ", groups=" + groups
				+ ", attributes=" + attributes + "]";
	}
}
