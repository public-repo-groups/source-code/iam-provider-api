package com.acme.component.iam.provider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.acme.component.iam.provider.entity.UserApp;
import com.acme.component.iam.provider.entity.UserRoleApp;

public interface UserRoleAppRepository extends JpaRepository<UserRoleApp, Long> {
	
	List<UserRoleApp> findByUserApp(UserApp userApp);

}
