package com.acme.component.iam.provider.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.acme.component.iam.provider.entity.UserApp;
import com.acme.component.iam.provider.entity.UserGroupApp;
import com.acme.component.iam.provider.entity.UserRoleApp;
import com.acme.component.iam.provider.repository.UserAppRepository;
import com.acme.component.iam.provider.repository.UserGroupAppRepository;
import com.acme.component.iam.provider.repository.UserRoleAppRepository;
import com.acme.component.iam.provider.rest.response.UserAppDetailRest;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/identity-provider-api")
public class IdentityProviderRestController {

	@Autowired
	UserAppRepository userAppRepository;

	@Autowired
	UserRoleAppRepository userRoleAppRepository;

	@Autowired
	UserGroupAppRepository userGroupAppRepository;

	@GetMapping("/get-user-data/{userId}")
	public ResponseEntity<UserAppDetailRest> getUserDataByUserId(@PathVariable("userId") long userId) {
		System.out.println("userId : " + userId);

		UserAppDetailRest userAppDetailRest = getUserAppDetailRestByUserId(userId);

		if (Objects.nonNull(userAppDetailRest)) {
			return new ResponseEntity<>(userAppDetailRest, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	private UserAppDetailRest getUserAppDetailRestByUserId(long userId) {
		System.out.println("Start getUserAppDetailRestByUserId");

		Optional<UserApp> userApp = userAppRepository.findById(userId);
		System.out.println("userApp : " + userApp.get().toString());

		List<String> roles = new ArrayList<String>();
		List<String> groups = new ArrayList<String>();

		List<UserRoleApp> userRoleAppList = userRoleAppRepository.findByUserApp(userApp.get());
		System.out.println("userRoleAppList.size() : " + userRoleAppList.size());
		for (UserRoleApp userRoleApp : userRoleAppList) {
			System.out.println("userRoleApp.getRoleApp() : " + userRoleApp.getRoleApp().toString());
			roles.add(userRoleApp.getRoleApp().getRole());
		}
		System.out.println("roles : " + roles);

		List<UserGroupApp> userGroupAppList = userGroupAppRepository.findByUserApp(userApp.get());
		System.out.println("userGroupAppList.size() : " + userGroupAppList.size());
		for (UserGroupApp userGroupApp : userGroupAppList) {
			System.out.println("userGroupApp.getGroupApp() : " + userGroupApp.getGroupApp().toString());
			groups.add(userGroupApp.getGroupApp().getGroup());
		}
		System.out.println("groups : " + groups);

		UserAppDetailRest userAppDetailRest = createUserAppDetailRest(userApp.get(), roles, groups);

		System.out.println("End getUserAppDetailRestByUserId");
		return userAppDetailRest;
	}

	private UserAppDetailRest createUserAppDetailRest(UserApp userApp, List<String> roles, List<String> groups) {

		UserAppDetailRest userAppDetailRest = new UserAppDetailRest();
		userAppDetailRest.setUserId(userApp.getId());
		userAppDetailRest.setUserName(userApp.getUserName());
		userAppDetailRest.setFirstName(userApp.getFirstName());
		userAppDetailRest.setLastName(userApp.getLastName());
		userAppDetailRest.setEmail(userApp.getEmail());
		userAppDetailRest.setRoles(roles);
		userAppDetailRest.setGroups(groups);
		userAppDetailRest.setAttributes(createStaticAttributes());

		System.out.println("roles : " + roles);

		return userAppDetailRest;
	}
	
	private List<Map<String, String>> createStaticAttributes(){
	
		List<Map<String, String>> attributes = new ArrayList<Map<String, String>>();
		Map<String, String> attribute = null;		
		
		attribute = new HashMap<String, String>();
		attribute.put("codigo", "MODULO.CONTABLEEJECUTORA");
		attribute.put("titulo", "CONTABLE - EJECUTORA");
		attribute.put("etiqueta", "Módulo Contable - Ejecutora");
		attribute.put("orden", "1");
		attributes.add(attribute);

		attribute = new HashMap<String, String>();
		attribute.put("codigo", "MODULO.CONTABLEPLIEGO");
		attribute.put("titulo", "CONTABLE - PLIEGO");
		attribute.put("etiqueta", "Módulo Contable - Pliego");
		attribute.put("orden", "2");
		attributes.add(attribute);

		attribute = new HashMap<String, String>();
		attribute.put("codigo", "MODULO.EJECUCION_PRESUPUESTAL");
		attribute.put("titulo", "EJECUCIÓN PRESUPUESTAL");
		attribute.put("etiqueta", "Módulo de Ejecución");
		attribute.put("orden", "3");
		attributes.add(attribute);

		attribute = new HashMap<String, String>();
		attribute.put("codigo", "MODULO.TICKET_SOPORTE");
		attribute.put("titulo", "TICKET DE SOPORTE");
		attribute.put("etiqueta", "Registro y Consulta de Tickets");
		attribute.put("orden", "4");
		attributes.add(attribute);
				
		return attributes;
	}

}
