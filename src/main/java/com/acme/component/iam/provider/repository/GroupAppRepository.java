package com.acme.component.iam.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.acme.component.iam.provider.entity.GroupApp;

public interface GroupAppRepository extends JpaRepository<GroupApp, Long> {

}
