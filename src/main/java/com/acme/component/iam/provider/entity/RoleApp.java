package com.acme.component.iam.provider.entity;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "role_app")
public class RoleApp implements Serializable {

	private static final long serialVersionUID = -6293662619886278744L;

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "role")
	private String role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "RoleApp [id=" + id + ", role=" + role + "]";
	}

}
