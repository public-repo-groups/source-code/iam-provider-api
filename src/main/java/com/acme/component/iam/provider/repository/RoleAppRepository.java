package com.acme.component.iam.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.acme.component.iam.provider.entity.RoleApp;

public interface RoleAppRepository extends JpaRepository<RoleApp, Long> {

}
