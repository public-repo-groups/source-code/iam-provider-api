package com.acme.component.iam.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.acme.component.iam.provider.entity.UserApp;

public interface UserAppRepository extends JpaRepository<UserApp, Long> {

}
