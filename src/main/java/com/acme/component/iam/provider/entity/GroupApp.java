package com.acme.component.iam.provider.entity;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "group_app")
public class GroupApp implements Serializable {

	private static final long serialVersionUID = 275589149669997947L;

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "group")
	private String group;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "RoleApp [id=" + id + ", group=" + group + "]";
	}

}
