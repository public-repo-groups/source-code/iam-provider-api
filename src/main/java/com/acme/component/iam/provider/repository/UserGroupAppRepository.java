package com.acme.component.iam.provider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.acme.component.iam.provider.entity.UserApp;
import com.acme.component.iam.provider.entity.UserGroupApp;

public interface UserGroupAppRepository extends JpaRepository<UserGroupApp, Long> {

	List<UserGroupApp> findByUserApp(UserApp userApp);
}
